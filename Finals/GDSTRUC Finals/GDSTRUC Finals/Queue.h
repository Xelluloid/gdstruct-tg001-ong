#pragma once
#include "UnorderedArray.h"
#include <string>

using namespace std;

template<class T>

class Queue
{
public:
	Queue(int size)
	{
		mArray = new UnorderedArray<T>(size);
	}

	void pushQueue(T value)
	{
		mArray->push(value);
	}

	void popQueue()
	{
		mArray->remove(0);
		mArray->pop();
	}

	void displayQueue()
	{
		for (int i = 0; i < (*mArray).getSize(); i++)
		{
			cout << (*mArray)[i] << endl;
		}
	}

	const T& topElementQueue()
	{
		return (*mArray)[0];
	}

	void removeArrayQueue()
	{
		for (int j = 0; j <= (*mArray).getSize(); j++)
		{
			mArray->remove(j);
		}
	}

private:
	UnorderedArray<T>*mArray;
};