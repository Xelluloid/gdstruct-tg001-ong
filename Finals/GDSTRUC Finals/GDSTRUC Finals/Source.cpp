#include <iostream>
#include <string>
#include "Queue.h"
#include "Stack.h"

using namespace std;

int main()
{
	int size;
	cout << "Enter size of list: ";
	cin >> size;

	Queue<int>Queue(size);
	Stack<int>Stack(size);

	system("pause");
	system("cls");

	while (true) 
	{
		int choice;
		cout << "What do you want to do?" << endl;
		cout << "[1] - Push" << endl;
		cout << "[2] - Pop" << endl;
		cout << "[3] - Print all" << endl;
		cin >> choice;
		
		if (choice == 1)
		{
			int number;
			cout << "Insert number: ";
			cin >> number;

			Queue.pushQueue(number);
			Stack.pushStack(number);

			cout << endl;
		}
		else if (choice == 2)
		{
			Queue.popQueue();
			Stack.popStack();

			cout << endl;
		}
		else if (choice == 3)
		{
			cout << "Queue : ";

			Queue.displayQueue();

			cout << "Stack : ";

			Stack.displayStack();

			Queue.removeArrayQueue();
			Stack.removeArrayStack();
		}

		cout << "Top of Element Sets: " << endl;
		cout << "Queue : " << Queue.topElementQueue() << endl;
		cout << "Stack : " << Stack.topElementStack() << endl;

		system("pause");
		system("cls");
	} ;

	system("pause");
	return 0;
}