#pragma once
#include "UnorderedArray.h"
#include <string>

using namespace std;

template<class T>

class Stack
{
public:
	Stack(int size)
	{
		mArray = new UnorderedArray<T>(size);
	}

	void pushStack(T value)
	{
		mArray->push(value);
	}

	void popStack()
	{
		mArray->remove((*mArray).getSize() - 1);
		mArray->pop();
	}

	void displayStack()
	{
		for (int i = (*mArray).getSize(); i > 0; i--)
		{
			cout << (*mArray)[i - 1] << endl;
		}
	}

	const T& topElementStack()
	{
		return (*mArray)[(*mArray).getSize() - 1];
	}

	void removeArrayStack()
	{
		for (int j = (*mArray).getSize(); j > 0; j--)
		{
			mArray->remove(j - 1);
		}
	}
private:
	UnorderedArray<T>*mArray;
};