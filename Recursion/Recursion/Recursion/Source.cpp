#include <iostream>
#include <string>

using namespace std;

int numberInput(int size)
{
	int number;
	cout << "Please input number: ";
	cin >> number;

	if (size < 0)
	{
		return number + numberInput(size - 1);
	}
}

int fiboSequence(int number)
{
	if ((number == 1) || (number == 0)) {
		return(number);
	}
	else {
		return(fiboSequence(number - 1) + fiboSequence(number - 2));
	}
}

bool isPrime(int number, int i)
{
	if (number < 2)
	{
		return 0;
	}
	if (i == 1)
	{
		return true;
	}
	else
	{
		if (number % i == 0)
		{
			return false;
		}
		else
		{
			return isPrime(number, i + 1);
		}
	}
}

int main() 
{
	cout << "=============[Addition]=============" << endl;
	int size;
	cout << "Enter size: ";
	cin >> size;

	int sum = numberInput(size);

	cout << "Sum: " << sum << endl;

	system("pause");
	system("cls");

	cout << "=============[Fibonacci Sequence]=============" << endl;
	int fiboSize;
	cout << "Enter size of sequence: ";
	cin >> fiboSize;

	int number = 0;

	while (number < fiboSize) 
	{
		cout << fiboSequence(number) << " ";
		number++;
	}

	system("pause");
	system("cls");

	cout << "=============[Prime Numbers]=============" << endl;
	int i = 2;
	int positiveInteger;
	
	cout << "Enter positive integer: ";
	cin >> positiveInteger;

	if (isPrime(positiveInteger, i))
	{
		cout << positiveInteger << " is a prime number." << endl;
	}
	else
	{
		cout << positiveInteger << " is not a prime number. " << endl;
	}

	system("pause");
	return 0;
}