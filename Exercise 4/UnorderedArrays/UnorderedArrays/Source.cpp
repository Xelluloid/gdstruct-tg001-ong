#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include "UnorderedArray.h"
using namespace std;

int userInputSize()
{
	int size;
	cout << "Input size of array: ";
	cin >> size;

	return size;
}

int userInputElement()
{
	int index;
	cout << "Choose an element to remove: ";
	cin >> index;

	return index;
}

int inputSearch()
{
	int searchItem;
	cout << "Input number to search: ";
	cin >> searchItem;

	return searchItem;
}

void main()
{
	srand(time(NULL));

	// asks user to input array size
	int inputSize = userInputSize();

	UnorderedArray<int> grades(inputSize);

	// fills array of inputSize with random values from 1 - 100
	for (int i = 1; i <= inputSize; i++)
		grades.push(rand() % 100 + 1);

	// prints array elements
	for (int i = 0; i < grades.getSize(); i++)
		cout << grades[i] << endl;

	// asks user which element to remove
	int inputElement = userInputElement();

	// removes element chosen
	grades.remove(inputElement);

	// prints array elements after removal of element
	for (int i = 0; i < grades.getSize(); i++)
		cout << grades[i] << endl;

	// searches the value.
	int searchValue = inputSearch();
	int result = grades.linearSearch(grades, inputSize, searchValue);

	// prints result of search.
	(result == -1) ? cout << "Value not found." << endl
		: cout << "Value found at index " << result + 1 << "." << endl;

	system("pause");
}