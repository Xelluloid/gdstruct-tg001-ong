#pragma once
#include <assert.h>

using namespace std;

template <class T> //allows UnorderedArray to take any data, T is a placeholder (can be anything)

class UnorderedArray
{
public:
	UnorderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0),
		mGrowSize(0), mNumElements(0) // growBy has a default value of 1, efficient to put all the parameters up here
	{
		if (size)
		{
			mMaxSize = size;

			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize); // allocates memory and sets 

			mGrowSize = ((growBy > 0) ? growBy : 0);
			// ? is a ternary operator. single line if else statement
			// if growBy > 0 then mGrowSize = growBy, else mGrowSize = 0
		}
	}

	virtual ~UnorderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
		{
			expand();
		}

		mArray[mNumElements] = value;
		mNumElements++;
		// if it is true, it continues
		// if not, assert will not continue
	}

	virtual T& operator[] (int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	int getSize()
	{
		return mNumElements;
	}

	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL);

		if (index >= mMaxSize) // index is greater than current size
			return;

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual int linearSearch(UnorderedArray<T> grades, int size, int search)
	{
		for (int i = 0; i < mMaxSize; i++)
		{
			if (grades[i] == search)
				return i;
		}
		return -1;
	}

private:
	T * mArray; // it is a generic type, Type T
	int mMaxSize;
	int mGrowSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
		{
			return false;
		}

		T *temp = new T[mMaxSize + mGrowSize];

		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;

		return true;
	}
};
