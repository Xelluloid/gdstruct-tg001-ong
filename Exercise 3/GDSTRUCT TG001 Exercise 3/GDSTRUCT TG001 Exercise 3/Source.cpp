#include <iostream>
#include <string>
#include <time.h>

using namespace std;

string inputGuildName()
{
	string guildName;

	cout << "Input name of guild: ";
	cin >> guildName;

	return guildName;
}

int size()
{
	int sizeValue;

	cout << "Input size of guild: ";
	cin >> sizeValue;

	return sizeValue;
}

void memberName(string* guildMember, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << "Input name of member " << i + 1 << ": ";
		cin >> *guildMember;

		guildMember++;
	}
}

void printMembers(string* guildMember, int size)
{
	for (int j = 0; j < size; j++)
	{
		cout << "Member " << j + 1 << ": " << *guildMember << endl;

		guildMember++;
	}
}

void system()
{
	system("pause");
	system("cls");
}

int option()
{
	int decision;

	cout << "Choose next option:" << endl;
	cout << "[1] Print information" << endl;
	cout << "[2] Change member name" << endl;
	cout << "[3] Add new member to database" << endl;
	cout << "[4] Delete member from database" << endl;
	cout << "[5] Exit database" << endl;
	cin >> decision;

	return decision;
}

void change(string* clan)
{
	int changeName;
	string nameChange;

	cout << "Choose member to change name:  " << endl;
	cin >> changeName;

	cout << "What's the new name? ";
	cin >> nameChange;

	clan[changeName - 1] = nameChange;
}

void addMember(string* clan, int size)
{
	string* temp = new string[size];

	for (int i = 0; i < size; i++)
	{
		temp[i] = clan[i];
	}

	delete[] clan;
	clan = NULL;

	int newMember;
	cout << "Enter number of new members: ";
	cin >> newMember;

	clan = new string[newMember];

	for (int j = 0; j < newMember; j++)
	{
		cout << "Input name of new member " << j + 1 << ": ";
		cin >> *clan;

		clan++;
	}

	int newSize = size + newMember;

	string* newGuild = new string[newSize];

	for (int k = 0; k < size; k++)
	{
		newGuild[k] = temp[k];
	}

	for (int l = size; l < newSize; l++)
	{
		newGuild[l] = clan[l - size];
	}

	delete[] clan;
	clan = NULL;
}

void removeMember(string* clan, int size)
{
	string member;
	cout << "Choose member to remove from database: ";
	cin >> member;

	for (int i = 0; i < size; i++)
	{
		if (clan[i] == member)
		{
			for (int j = i; j < size - 1; j++)
			{
				clan[j] = clan[j + 1];
			}

			clan[size - 1] = " ";
			size--;
			return;
		}
	}
}

int main()
{
	srand(time(NULL));

	string guildName = inputGuildName();
	int guildSize = size();
	int choice = 0;
	string* guild = new string[guildSize];

	memberName(guild, guildSize);

	system();

	while (true)
	{
		choice = option();
		system();

		switch (choice)
		{
		case 1:
			cout << "Guild name: " << guildName << endl;
			cout << "================" << endl;
			printMembers(guild, guildSize);
			break;
		case 2:
			change(guild);
			break;
		case 3:
			addMember(guild, guildSize);
			break;
		case 4:
			removeMember(guild, guildSize);
			break;
		case 5:
			cout << "Thank you for your time." << endl;

			system("pause");
			return 0;
		}

		system();
	}
}